*** Settings ***
Library     RequestsLibrary
Library     Collections
Library     JSONLibrary

*** Variables ***
${Base_url}     https://dapi.docquity.com
#${actual_code}      { "status_code": { "enum": [200, 201, 204, 400] } }
${response}     reture_status_code
${headers}       Create Dictionary      Content-Type=aplication/json        lang=en     devicetype=web      udid=1234       ver=4.1     Authorization=Basic SDJPSDJTTzRPT0hDSDRIMk8yOg==
authorization       "SDJPSDJTTzRPT0hDSDRIMk8yOm54WHU2ZWVSbnM5NnlBNg=="
${Status}
*** Test Cases ***
Pass Get Request
    [Tags]  API_Webinar
    Create Session  Get_validation    ${Base_url}
    ${response}=        Get Request     Get_validation      /4.1/webinar/agoratoken/v2?product_type=27&product_type_id=882
Response Status should be Success 200,201,204
    ${actual_code}=     convert to string       ${response.status_code}      headers=        ${headers}
    #BuiltIn.Log To Console       ${response.content}
    #BuiltIn.Log To Console       ${response.status_code}
    should be equal     ${actual_code}      200     201

